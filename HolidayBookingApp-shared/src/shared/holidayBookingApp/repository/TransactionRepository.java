/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared.holidayBookingApp.repository;

import java.util.List;
import javax.ejb.Remote;
import shared.holidayBookingApp.entities.Transaction;

/**
 *
 * @author Vijju
 */
@Remote
public interface TransactionRepository {
    
    /**
     * Add the transaction being passed as parameter into the repository
     *
     * @param transaction - the transaction to add
     * @throws java.lang.Exception
     */
    public void addTransaction(Transaction transaction) throws Exception;
    
    /**
     * Search for a transaction by its transaction ID
     *
     * @param id - the transactionId of the transaction to search for
     * @return the transaction found
     * @throws java.lang.Exception
     */
    public Transaction searchTransactionById(int id) throws Exception;
    
    /**
     * Return all the transactions in the repository
     *
     * @return all the transactions in the repository
     * @throws java.lang.Exception
     */
    public List<Transaction> getAllTransactions() throws Exception;
    
     /**
     * Remove the transaction, whose transaction ID matches the one being passed as parameter, from the repository
     *
     * @param transactionId - the ID of the transaction to remove
     * @throws java.lang.Exception
     */
    public void removeTransaction(int transactionId) throws Exception;
    
    /**
     * Update a transaction in the repository
     *
     * @param transaction - the updated information regarding a transaction
     * @throws java.lang.Exception
     */
    public void editTransaction(Transaction transaction) throws Exception;
}
