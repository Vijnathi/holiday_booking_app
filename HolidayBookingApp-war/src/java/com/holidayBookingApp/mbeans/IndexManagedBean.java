/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holidayBookingApp.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import shared.holidayBookingApp.entities.Address;
import shared.holidayBookingApp.entities.Helper;
import shared.holidayBookingApp.entities.User;
import shared.holidayBookingApp.repository.HelperRepository;
import shared.holidayBookingApp.repository.UserRepository;

/**
 *
 * @author Vijju
 */
@ManagedBean(name = "indexManagedBean")
@ApplicationScoped
public class IndexManagedBean implements Serializable {

    /**
     * Creates a new instance of IndexManagedBean
     */
    private String appTitle;
    private String pageTitle;
    private String username;
    private String password;
    private String pass;
    
    private User user;
    private Address address;
    
    private Helper helper;
    private HelperRepository helperRepository;
    
    private HttpServletRequest httpServletRequest;
//    private Principal p;
            
    @EJB
    private UserRepository userRepository;
    
    public IndexManagedBean() {
        this.appTitle = "Holiday Booking Enterprise Application";
        this.pageTitle = "Holiday Boutique";
        
        this.user = new User();
        this.address = new Address();
        this.helper = new Helper();
//        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        this.helper.setCurrentUser(request.getRemoteUser());
        
//        this.p = this.httpServletRequest.getUserPrincipal();
//        this.helper.setCurrentUser(this.p.getName());
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getAppTitle() {
        return appTitle;
    }

    public void setAppTitle(String appTitle) {
        this.appTitle = appTitle;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Helper getHelper() {
        return helper;
    }

    public void setHelper(Helper helper) {
        this.helper = helper;
    }

    public HelperRepository getHelperRepository() {
        return helperRepository;
    }

    public void setHelperRepository(HelperRepository helperRepository) {
        this.helperRepository = helperRepository;
    }
    
    
    public String gotoRegisterForm() {
        return "register.xhtml?faces-redirect=true";
//        FacesContext context = FacesContext.getCurrentInstance();
//        HttpServletRequest origRequest = (HttpServletRequest)context.getExternalContext().getRequest();
//        String contextPath = origRequest.getContextPath();
//        try {
//            FacesContext.getCurrentInstance().getExternalContext()
//                .redirect("http://localhost:8080/HolidayBookingApp-war/faces/register.xhtml");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
    
    public String addUser(){
        MessageDigest digest;
        byte[] hash;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            
            this.user.setPassword(hexString.toString());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(IndexManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        this.user.setAddress(this.address);
        this.user.setType("Public");
        try {
            userRepository.addUser(this.user);
        } catch (Exception ex) {
            Logger.getLogger(IndexManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       return "index.xhtml?faces-redirect=true";
    }
    
    public void doLogin(){
//        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        if(request != null){
//        this.helper.setCurrentUser(request.getRemoteUser());
//        }
//          this.helper.setCurrentUser(httpServletRequest.getRemoteUser());
            System.out.println("com.holidayBookingApp.mbeans.IndexManagedBean.doLogin()");
//        this.helper.setCurrentUser(username);
        
    }
//    public String doLogin(){
//        try {
//            User loginUser = userRepository.findUser(username, password);
//            if("P".equals(loginUser.getType())){
//                helper.setCurrentUser("Public");
//            } else {
//                helper.setCurrentUser("Worker");
//            }
//            return "home.xhtml?faces-redirect=true";
//        } catch (Exception ex) {
//            Logger.getLogger(IndexManagedBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }
    
    private boolean responseRendered = false;

    
    public void findUser() {
        responseRendered = !responseRendered;
        System.out.println("finding..... ("+ responseRendered+")" + this);
    }
    public boolean isResponseRendered() {
        return responseRendered;
    }

    public void setResponseRendered(boolean responseRendered) {
        this.responseRendered = responseRendered;
    }       
}
