/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holidayBookingApp.admin.mbeans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import shared.holidayBookingApp.entities.User;
import shared.holidayBookingApp.repository.UserRepository;

/**
 *
 * @author Lucky
 */
@Named(value = "adminUsersManagedBean")
@SessionScoped
public class AdminUsersManagedBean implements Serializable {

    /**
     * Creates a new instance of UsersManagedBean
     */
    @EJB
    private UserRepository userRepository;
    
    public AdminUsersManagedBean() {
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
    public List<User> getAllUsers() {
        try {
            return userRepository.getAllUsers();
        } catch (Exception ex) {
            Logger.getLogger(AdminUsersManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
