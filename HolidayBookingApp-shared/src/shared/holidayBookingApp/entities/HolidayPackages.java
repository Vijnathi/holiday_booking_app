/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared.holidayBookingApp.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;

/**
 *
 * @author Vijju
 */
@Entity
@NamedQueries({
    @NamedQuery(name = HolidayPackages.GET_ALL_HOLIDAY_PACKAGES, query = "SELECT p FROM HolidayPackages p")})
public class HolidayPackages implements Serializable{
    
    public static final String GET_ALL_HOLIDAY_PACKAGES = "HolidayPackages.getAllHolidayPackages";
    private int packageId;
    private String packageName;
    //indicates couple, group, individual
    private String packageType;
    private String description;
    private int noOfPeople;
    
    Set<Available> availability;
    Set<Transaction> transactions;
    
    public HolidayPackages() {
    }

    public HolidayPackages(int packageId, String packageName, String packageType, String description, int noOfPeople) {
        this.packageId = packageId;
        this.packageName = packageName;
        this.packageType = packageType;
        this.description = description;
        this.noOfPeople = noOfPeople;
        this.availability = new HashSet<>();
        this.transactions = new HashSet<>();
    }

    @TableGenerator(name = "package_id_generator", table = "package_id_generator", pkColumnName = "gen_name", valueColumnName = "gen_val", allocationSize = 100)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "package_id_generator")
    @Column(name = "PACKAGE_ID")
    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    @Column(name = "PACKAGE_NAME")
    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @Column(name = "PACKAGE_TYPE")
    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }
    
    @Lob
    @Column
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "NO_OF_PEOPLE")
    public int getNoOfPeople() {
        return noOfPeople;
    }

    public void setNoOfPeople(int noOfPeople) {
        this.noOfPeople = noOfPeople;
    }

    /**
     *
     * @return
     */
    @OneToMany(mappedBy = "holidayPackage")
    public Set<Available> getAvailability() {
        return availability;
    }

    /**
     *
     * @param availability
     */
    public void setAvailability(Set<Available> availability) {
        this.availability = availability;
    }

    @OneToMany(mappedBy = "holidayPackages")
    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }
    
}
