/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared.holidayBookingApp.repository;

import java.util.List;
import javax.ejb.Remote;
import shared.holidayBookingApp.entities.User;

/**
 *
 * @author Vijnathi
 */
@Remote
public interface UserRepository {
    
    /**
     * Add the user being passed as parameter into the repository
     *
     * @param user - the user to add
     * @throws java.lang.Exception
     */
    public void addUser(User user) throws Exception;
    
    /**
     * Find the user using username and password for login
     *
     * @param username - the email of the user to search for
     * @param password - the password of the user to search for
     * @return the user found
     * @throws java.lang.Exception
     */
    public User findUser(String username, String password) throws Exception;
    
    /**
     * Search for a user by its user ID
     *
     * @param id - the userId of the user to search for
     * @return the user found
     * @throws java.lang.Exception
     */
    public User searchUserById(int id) throws Exception;
    
    /**
     * Return all the users in the repository
     *
     * @return all the users in the repository
     * @throws java.lang.Exception
     */
    public List<User> getAllUsers() throws Exception;
    
     /**
     * Remove the user, whose user ID matches the one being passed as parameter, from the repository
     *
     * @param userId - the ID of the user to remove
     * @throws java.lang.Exception
     */
    public void removeUser(int userId) throws Exception;
    
    /**
     * Update a user in the repository
     *
     * @param user - the updated information regarding a user
     * @throws java.lang.Exception
     */
    public void editUser(User user) throws Exception;
    
}
