INSERT INTO HOLIDAYBOOKINGAPPLICATION.HOTELS (HOTEL_ID, DESCRIPTION, HOTEL_NAME, LOCATION, SHORT_DESCRIPTION) 
    VALUES ('H001', 'Just 4 km south of Melbourne''s central business district, Quest Windsor offers modern apartment-style accommodations. Apartments include well-equipped kitchens and laundry facilities.
        The spacious apartments at Quest Windsor feature modern amenities including satellite TV, a DVD player and tea/coffee maker. The tiled bathrooms have a walk-in shower and bathroom amenities. Most apartments also has a private outdoor balcony 
        and large lounge area. Quest Windsor''s staff can arrange for babysitting or laundry services. Sightseeing tours can be arranged at the hotel''s tour desk. Meeting facilities are also available. The hotel is situated a 5-minute walk from Albert Park. 
        Prahran Train Station is just a 10 minute walk from the hotel. Tullamarine Airport is a 20-minute drive from Quest Windsor. Prahran is a great choice for travelers interested in food, coffee and cafes.',
    'Quest Windsor', '111 Punt Road, Prahran, 3181 Melbourne, Australia', 'Modern apartment-style accommodations');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.HOTELS (HOTEL_ID, DESCRIPTION, HOTEL_NAME, LOCATION, SHORT_DESCRIPTION) 
    VALUES ('H002', 'Melbourne SkyHigh Apartments offers high-rise apartments with great views over the surrounding areas. Guests enjoy a sauna, a fitness center and an indoor swimming pool. 
        Etihad Stadium is 701 m away. Each of the apartments offers a fully equipped kitchen, a dining setting and a lounge area with a flat-screen TV and DVD player. There is a private bathroom with a shower and free toiletries in every unit. Towels are provided.
        Melbourne Convention and Exhibition Center is 801 m from Melbourne SkyHigh Apartments, while Eureka Tower is 901 m from the property.The nearest airport is Tullamarine Airport, 12 mi from Melbourne SkyHigh Apartments.', 
    'Melbourne SkyHigh Apartments', '568 Collins Street, 3000 Melbourne, Australia', 'High-rise apartments with great views over the surrounding areas');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.HOTELS (HOTEL_ID, DESCRIPTION, HOTEL_NAME, LOCATION, SHORT_DESCRIPTION) 
    VALUES ('H003', 'This Travelodge is in Melbourne''s Docklands area, just 197 m from Etihad Stadium. It offers air-conditioned rooms with satellite TV and private bathrooms. Guests receive 100 MBs of free Wi-Fi per 24 hours.
                    All rooms have internet accessible work desks, a kitchenette with a microwave and tea/coffee-making facilities.
                    Breakfast is available in the bright, modern breakfast room each morning. The hotel is also close to a wide variety of restaurants and cafes on Collins Street.
                    The Travelodge Docklands is just a 5-minute walk from Southern Cross Train Station and the Collins Street Tram Stop. Melbourne International Airport is just a 20-minute drive away. 
                    Docklands is a great choice for travelers interested in sports, convenient public transportation and clothes shopping.
                    This is our guests'' favorite part of Melbourne, according to independent reviews.', 
                'Travelodge Hotel Melbourne Docklands','66 Aurora Lane, Docklands, 3008 Melbourne, Australia', 'Great choice for travelers interested in sports, convenient public transportation and clothes shopping');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.HOTELS (HOTEL_ID, DESCRIPTION, HOTEL_NAME, LOCATION, SHORT_DESCRIPTION) 
    VALUES ('H004', 'Mantra St Kilda offers luxury designer apartments with modern amenities, including iPod docking station and LCD TV. Facilities include a fitness center, restaurant and bar.
                    Each apartment at Mantra St Kilda includes a private balcony, private bathroom, and individual heating and cooling system. The modern kitchenette includes a full-size refrigerator, microwave and dishwasher.
                    24-hour reception and in-room dining are available, along with internet access and luggage storage. Guest laundry facilities and available and secure private parking is available for an additional charge of $25 per day. The hotel offers an airport transfer service upon prior arrangement, for an additional charge.
                    Surrounded by parks and gardens, only 500 m from Albert Park Lake, Mantra St Kilda is 10 minutes by tram or car from Melbourne city center. It is a 15-minute walk to Chapel Street shopping precinct.
                    The onsite restaurant Bakini serves Italian cuisine using fresh local produce. Enjoy a delicious meal or make the most of happy hour specials from 17:00 until 18:30.', 
                'Mantra St Kilda Road','568 St Kilda Road, 3004 Melbourne, Australia', 'Luxury designer apartments with modern amenities, including iPod docking station');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.HOTELS (HOTEL_ID, DESCRIPTION, HOTEL_NAME, LOCATION, SHORT_DESCRIPTION) 
    VALUES ('H005', 'Just 8 minutes'' walk from the MCG, Rod Laver Arena and Melbourne''s Olympic Park, Adara Richmond offers self-contained apartments with flat-screen cable TV and a DVD player. Guests can maintain their workout routine in the furbished fitness center.
                    Adara Richmond is directly opposite The Epworth Hospital. The apartments are 7 minutes'' drive from central Melbourne, and 15 minutes'' drive from St Kilda Beach. Melbourne Airport is a 25-minute drive away.
                    The air-conditioned apartments include a kitchen with a stove, a microwave and a refrigerator. Each apartment also has laundry facilities. Apartments with balconies are available on request.
                    Breakfast supplies are also available upon request, including cereal, juice, milk and muffins. The property also offers meeting and conference facilities. 
                    Richmond is a great choice for travelers interested in sports, cafes and restaurants.', 
                'Adara Richmond','185 Lennox Street, Richmond, 3121 Melbourne, Australia', 'dara Richmond offers self-contained apartments with flat-screen cable TV and a DVD player');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.HOTELS (HOTEL_ID, DESCRIPTION, HOTEL_NAME, LOCATION, SHORT_DESCRIPTION) 
    VALUES ('H006', 'Located in the heart of Hawthorn, Quest Apartments offers self-contained accommodations just 400 m from Glenferrie Railway Station. The Melbourne Central Business District (CBD) is an 11-minute train ride away. Rod Laver Arena and the Melbourne Cricket Ground (MCG) are both less than a 15-minute drive.
                    The property features a well-equipped fitness center, a sunny terrace and barbecue facilities.
                    All air-conditioned units include a well-equipped kitchenette with a microwave and refrigerator. Each unit has a spacious seating area with a work desk, flat-screen TV and iPod docking station. Most units also have a private balcon', 
                'Quest Hawthorn','616 Glenferrie Road, 3122 Melbourne, Australia', 'Located in the heart of Hawthorn, Quest Apartments offers self-contained accommodations');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.HOTELS (HOTEL_ID, DESCRIPTION, HOTEL_NAME, LOCATION, SHORT_DESCRIPTION) 
    VALUES ('H007', 'Centrally located in the central business district, this 4.5-star registered historic hotel has been restored to retain the feel and style of its 1913 origins while providing guests with modern technology and facilities. Guests receive 100MBs of free WiFi per 24 hours.
                    Distinguished by elegance, historic significance and traditional luxury, Rendezvous Hotel Melbourne offers 340 elegantly furnished guest rooms and suites. Each includes individually controlled air conditioning, cable TV, a private bathroom and a private electronic safe.
                    This award-winning hotel is within walking distance of world-class shopping, theaters, sports venues, entertainment and dining. Federation Square and the historic Flinders Street Train Station are right across from the hotel.
                    Straits Café offers buffet breakfast, and an a la carte dinner menu with international cuisine. Room service is available 24/7.', 
                'Rendezvous Hotel Melbourne','328 Flinders Street, 3000 Melbourne, Australia', 'Distinguished by elegance, historic significance and traditional luxury, Rendezvous Hotel Melbourne offers 340 elegantly furnished guest rooms and suites.');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.HOTELS (HOTEL_ID, DESCRIPTION, HOTEL_NAME, LOCATION, SHORT_DESCRIPTION) 
    VALUES ('H008', 'Quest Moonee Valley is located directly across the road from the Moonee Valley Race Course and a short walk to the vibrant Puckle Street shopping and restaurant precinct.
                    These spacious apartments are a 15-minute drive from Tullamarine Airport and a short tram or train ride away from Melbourne CBD (Central Business District). Quest Moonee Valley is located near the State Hockey and Netball Center and a short walk away from the Clocktower Center on Mount Alexander Road.
                    The apartments feature a kitchen or a kitchenette. Some have a balcony and a seating area. All of the 1, 2 and 3-bedroom apartments include their own laundry facilities there is also a communal laundry available to use.
                    You can prepare a meal in the communal barbecue area and have a swim in our outdoor pool.', 
                'Quest Moonee Valley','1 McPherson Street, 3039 Melbourne, Australia', 'Quest Moonee Valley is located directly across the road from the Moonee Valley Race Course. These spacious apartments are a 15-minute drive from Tullamarine Airport.');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.HOTELS (HOTEL_ID, DESCRIPTION, HOTEL_NAME, LOCATION, SHORT_DESCRIPTION) 
    VALUES ('H009', 'Located in the heart of Melbourne''s CBD, Punthill Manhattan offers stylish self-contained apartments with satellite TV. Facilities include a fitness center and spa pool.
                    Punthill Apartment Hotel Manhattan is 10 minutes'' walk from Flinders Street Station and the Yarra River. Rod Laver Arena is a 15-minute walk away.
                    All apartments are air-conditioned and include laundry facilities, plus a fully equipped kitchen with dishwasher and microwave. Each apartment has a spacious living area. 
                    This is our guests'' favorite part of Melbourne, according to independent reviews.', 
                'Punthill Apartment Hotel - Manhattan','57 Flinders Lane, 3000 Melbourne, Australia', 'Punthill Manhattan offers stylish self-contained apartments with satellite TV.');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.HOTELS (HOTEL_ID, DESCRIPTION, HOTEL_NAME, LOCATION, SHORT_DESCRIPTION) 
    VALUES ('H010', 'Located in the Melbourne CBD district in Melbourne, Nest-Apartments Sea View Apartments offers a garden and sauna. A Coles supermarket is 50 m away, the Sky Bus Station at Southern Cross Train Station is 100 m away and Etihad Stadium is 500 m away. Private parking is available on site.
                    All units are air conditioned and have a seating area. There is also a kitchen, equipped with a dishwasher, oven and microwave. A toaster and fridge are also offered, as well as a kettle. Nest-Apartments Sea View Apartments features free WiFi throughout the property. Towels are featured.
                    Nest-Apartments Sea View Apartments also includes a fitness center.
                    Melbourne Convention and Exhibition Center is 0.7 mi from Nest-Apartments Sea View Apartments, while Eureka Tower is 0.7 mi from the property. The nearest airport is Melbourne Airport, 12 mi from the property.', 
                'Nest-Apartments', '220 Spencer St., 3000 Melbourne, Australia', 'Sea View Apartments');

SELECT * FROM HOLIDAYBOOKINGAPPLICATION.HOTELS FETCH FIRST 100 ROWS ONLY;
