INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R001', '2', 'No. of Beds: 1 Queen Bed; Apartment Size: 36 sqm; Apartment Facilities: Balcony, TV, Telephone, DVD Player, Satellite channels,
                Cable channels, Flat-screen TV, Air conditioning, Iron, Ironing facilities, Sitting area, Washing machine,
                Heating, Sofa, Soundproof, Dryer, Wardrobe/Closet, Drying rack for clothing, Shower, Hairdryer, Free toiletries,
                Toilet, Private Bathroom, Bathtub or shower, Toilet paper, Tea/Coffee maker, Refrigerator, Microwave, Kitchen,
                Dining area, Electric kettle, Kitchenware, Oven, Stovetop, Toaster, Dining table, High chair, Towels, Linens,
                Upper floors accessible by elevator, Private apartment in building, Toilet With Grab Rails, Walk-in Shower,
                Raised Toilet, Shower Chair. Free WiFi!', 5,
            'One-Bedroom Apartment', 'Available', 'H001');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R002', '4', 'No. of Beds: 2 Queen Beds; Apartment Size: 80 sqm; Apartment Facilities: Balcony, Patio, TV, Telephone, DVD Player, Satellite channels,
                Cable channels, Flat-screen TV, Air conditioning, Iron, Ironing facilities, Sitting area, Washing machine,
                Heating, Sofa, Soundproof, Dryer, Wardrobe/Closet, Fold-up bed, Drying rack for clothing, Shower, Bathtub, Hairdryer, Free toiletries,
                Toilet, Private Bathroom, Bathtub or shower, Additional bathroom, Toilet paper, Tea/Coffee maker, Refrigerator, Microwave, Kitchen,
                Dining area, Electric kettle, Kitchenware, Oven, Stovetop, Toaster, Dining table, High chair, Towels, Linens,
                Upper floors accessible by elevator, Private apartment in building, Toilet With Grab Rails, Walk-in Shower,
                Raised Toilet, Shower Chair. Free WiFi!', 5,
            'Two-Bedroom Apartment', 'Available', 'H001');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R003', '2', 'No. of Beds: 1 Queen Beds; Apartment Size: 45 sqm; Apartment Facilities: Landmark view, TV, Telephone, Flat-screen TV, Air conditioning, Iron, 
                Ironing facilities, Sitting area, Washing machine, Heating, Sofa, Soundproof, Dryer, Wardrobe/Closet, Shower, Hairdryer, Free toiletries,
                Toilet, Private Bathroom, Bathtub or shower, Toilet paper, Kitchenette, Refrigerator, Microwave, Kitchen, Electric kettle, Kitchenware, Oven, Stovetop, 
                Toaster, Dining table, Towels, Linens, Upper floors accessible by elevator, Walk-in Shower. Free WiFi!', 10, 
            'Standard One-Bedroom Apartment', 'Available', 'H002');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R004', '2/3/4', 'No. of Beds: 1 Queen Bed and 2 Twin Beds; Apartment Size: 60 sqm; Apartment Facilities: Landmark view, TV, Telephone, Flat-screen TV, Air conditioning, Iron, 
                Ironing facilities, Sitting area, Washing machine, Heating, Sofa, Soundproof, Dryer, Wardrobe/Closet, Shower, Hairdryer, Free toiletries,
                Toilet, Private Bathroom, Bathtub or shower, Toilet paper, Kitchenette, Refrigerator, Microwave, Kitchen, Electric kettle, Kitchenware, Oven, Stovetop, 
                Toaster, Dining table, Towels, Linens, Upper floors accessible by elevator, Walk-in Shower. Free WiFi!', 4,
            'Standard Two-Bedroom Apartment', 'Available', 'H002');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R005', '2/3/4', 'No. of Beds: 1 Queen Bed and 2 Twin Beds; Apartment Size: 60 sqm; Apartment Facilities: Landmark view, TV, Telephone, Flat-screen TV, Air conditioning, Iron, 
                Ironing facilities, Sitting area, Washing machine, Heating, Sofa, Soundproof, Dryer, Wardrobe/Closet, Shower, Hairdryer, Free toiletries,
                Toilet, Private Bathroom, Bathtub or shower, Additional bathroom, Toilet paper, Kitchenette, Refrigerator, Microwave, Kitchen, Electric kettle, Kitchenware, Oven, Stovetop, 
                Toaster, Dining table, Towels, Linens, Upper floors accessible by elevator, Walk-in Shower. Free WiFi!', 5,
            'Deluxe Two-Bedroom Apartment', 'Available', 'H002');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R006', '2', 'No. of Beds: 1 Queen Bed; Apartment Size: 42 sqm; Apartment Facilities: Landmark view, TV, Telephone, Flat-screen TV, Air conditioning, Iron, 
                Ironing facilities, Sitting area, Washing machine, Heating, Sofa, Soundproof, Dryer, Wardrobe/Closet, Shower, Hairdryer, Free toiletries,
                Toilet, Private Bathroom, Bathtub or shower, Toilet paper, Kitchenette, Refrigerator, Microwave, Kitchen, Electric kettle, Kitchenware, Oven, Stovetop, 
                Toaster, Dining table, Towels, Linens, Upper floors accessible by elevator, Walk-in Shower. Free WiFi!', 1,
            'Standard One-Bedroom Apartment', 'Available', 'H002');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R007', '2', 'No. of Beds: 1 Queen Bed; Apartment Size: 27 sqm; Apartment Facilities: Tea/Coffee maker, Shower, Safe, TV, Telephone, Air conditioning, Hairdryer, Iron,
                Kitchenette, Refrigerator, Ironing facilities, Free toiletries, Toilet, Micorwave, Private bathroom, Heating, Satellite channels, Cable channels, bathtub or shower,
                Flat-screen TV, Sofa, Wake-up service, Electric kettle, Wardrobe/Closet, Towels, Linens, Upper floors accessible by elevator, Entire unit wheelchair accessible,
                Toilet paper, Walk-in Shower. Free WiFi!', 3,
            'Standard King Room', 'Available', 'H003');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R008', '2', 'No. of Beds: 2 Twin Beds; Apartment Size: 27 sqm; Apartment Facilities: Tea/Coffee maker, Shower, Safe, TV, Telephone, Air conditioning, Hairdryer, Iron,
                Kitchenette, Refrigerator, Ironing facilities, Free toiletries, Toilet, Micorwave, Private bathroom, Heating, Satellite channels, Cable channels, bathtub or shower,
                Flat-screen TV, Sofa, Wake-up service, Electric kettle, Wardrobe/Closet, Towels, Linens, Upper floors accessible by elevator, Entire unit wheelchair accessible,
                Toilet paper, Walk-in Shower. Free WiFi!', 8,
            'Standard Twin Room', 'Available', 'H003');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R009', '3', 'No. of Beds: 3 Twin Beds; Apartment Size: 27 sqm; Apartment Facilities: Tea/Coffee maker, Shower, Safe, TV, Telephone, Air conditioning, Hairdryer, Iron,
                Kitchenette, Refrigerator, Ironing facilities, Free toiletries, Toilet, Micorwave, Private bathroom, Heating, Satellite channels, Cable channels, bathtub or shower,
                Flat-screen TV, Sofa, Wake-up service, Electric kettle, Wardrobe/Closet, Towels, Linens, Upper floors accessible by elevator, Entire unit wheelchair accessible,
                Toilet paper, Walk-in Shower. Free WiFi!', 5,
            'Standard Triple Room', 'Available', 'H003');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R010', '2', 'No. of Beds: 1 King Bed; Apartment Size: 29 sqm; Apartment Facilities: Balcony, Telephone, Flat-screen TV, iPod dock, Air conditioning, Iron,
                Ironing facilities, Sitting area, Heating, Shower, Hairdryer, Free toiletries, Toilet, Private Bathroom, Minibar, Kitchenette, Refrigerator, Microwave, 
                Electric kettle, Kitchenware, Wake-up service. Free WiFi!', 2,
            'Studio', 'Available', 'H004');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R011', '2', 'No. of Beds: 1 King Bed; Apartment Size: 39 sqm; Apartment Facilities: Tea/Coffee maker, Shower, Safe, TV, Telephone, Air conditioning, Hairdryer, Iron,
                Kitchenette, Refrigerator, Ironing facilities, Free toiletries, Toilet, Micorwave, Private bathroom, Heating, Satellite channels, Cable channels, bathtub or shower,
                Flat-screen TV, Sofa, Wake-up service, Electric kettle, Wardrobe/Closet, Towels, Linens, Upper floors accessible by elevator, Entire unit wheelchair accessible,
                Toilet paper, Walk-in Shower. Free WiFi!', 4,
            'Executive Studio', 'Available', 'H004');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R012', '4', 'No. of Beds: 2 King Beds; Apartment Size: 27 sqm; Apartment Facilities: Tea/Coffee maker, Shower, Safe, TV, Telephone, Air conditioning, Hairdryer, Iron,
                Kitchenette, Refrigerator, Ironing facilities, Free toiletries, Toilet, Micorwave, Private bathroom, Heating, Satellite channels, Cable channels, bathtub or shower,
                Flat-screen TV, Sofa, Wake-up service, Electric kettle, Wardrobe/Closet, Towels, Linens, Upper floors accessible by elevator, Entire unit wheelchair accessible,
                Toilet paper, Walk-in Shower. Free WiFi!', 7,
            'Two-Bedroom Apartment', 'Available', 'H004');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R013', '4/5', 'No. of Beds: 1 King Bed and 1 Queen Bed; Apartment Size: 27 sqm; Apartment Facilities: Tea/Coffee maker, Shower, Safe, TV, Telephone, Air conditioning, Hairdryer, Iron,
                Kitchenette, Refrigerator, Ironing facilities, Free toiletries, Toilet, Micorwave, Private bathroom, Heating, Satellite channels, Cable channels, bathtub or shower,
                Flat-screen TV, Sofa, Wake-up service, Electric kettle, Wardrobe/Closet, Towels, Linens, Upper floors accessible by elevator, Entire unit wheelchair accessible,
                Toilet paper, Walk-in Shower. Free WiFi!', 5,
            'Family Two-Bedrrom Apartment', 'Available', 'H004');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R014', '2', 'No. of Beds: 2 Twin Beds; Apartment Size: 34 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Iron, Kitchenette,
                Refrigerator, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 10,
            'Studio Apartment', 'Available', 'H005');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R015', '2/3', 'No. of Beds: 1 Queen Bed; Apartment Size: 44 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Iron, Kitchenette,
                Refrigerator, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 9,
            'One-Bedroom Apartment', 'Available', 'H005');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R016', '2/3', 'No. of Beds: 1 Queen Bed; Apartment Size: 54 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Iron, Kitchenette,
                Refrigerator, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 5,
            'Executive One-Bedroom Apartment', 'Available', 'H005');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R017', '4', 'No. of Beds: 2 Queen Beds; Apartment Size: 34 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Iron, Kitchenette,
                Refrigerator, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 6,
            'Two-Bedroom Apartment', 'Available', 'H005');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R018', '2', 'No. of Beds: 2 Twin Beds or 1 King Bed; Apartment Size: 34 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Satellite Channels, Iron, Kitchenette,
                Refrigerator, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 8,
            'Studio', 'Available', 'H006');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R019', '2', 'No. of Beds: 2 Twin Beds or 1 King Bed; Apartment Size: 34 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Satellite Channels, Iron, Kitchenette,
                Refrigerator, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 5,
            'Executive Studio', 'Available', 'H006');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R020', '2', 'No. of Beds: 2 Twin Beds; Apartment Size: 34 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Satellite Channels, Iron, Kitchenette,
                Refrigerator, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 5,
            'Guest Twin Room', 'Available', 'H007');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R021', '2/3', 'No. of Beds: 1 Queen Bed; Apartment Size: 34 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Satellite Channels, Iron, Kitchenette,
                Refrigerator, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 7,
            'Deluxe Queen Room', 'Available', 'H007');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R022', '2', 'No. of Beds: 1 Queen Bed; Apartment Size: 34 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Satellite Channels, Iron, Kitchenette,
                Refrigerator, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 3,
            'Deluxe Queen Room with River View', 'Available', 'H007');


INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R023', '2/3', 'No. of Beds: 1 Queen Bed; Apartment Size: 34 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Satellite Channels, Iron, Kitchenette,
                Refrigerator, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 4,
            'Heritage Double Room', 'Available', 'H007');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R024', '2/3', 'No. of Beds: 1 King Bed; Apartment Size: 48 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Satellite Channels, Iron, Kitchenette,
                Refrigerator, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 6,
            'Deluxe King Suite', 'Available', 'H007');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R025', '2', 'No. of Beds: 1 Queen Bed; Apartment Size: 46 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Satellite Channels, Iron, Kitchenette,
                Refrigerator, Patio, Barbecue, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 5,
            'One-Bedroom Apartment with Balcony', 'Available', 'H008');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R026', '4', 'No. of Beds: 1 Queen Bed and 2 Twin Beds; Apartment Size: 48 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Satellite Channels, Iron, Kitchenette,
                Refrigerator, Patio, Barbecue, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 8,
            'Two-Bedroom Apartment with Balcony', 'Available', 'H008');

INSERT INTO HOLIDAYBOOKINGAPPLICATION.ROOMS (ROOM_ID, CAPACITY, DESCRIPTION, NO_OF_ROOMS, ROOM_TYPE, STATUS, HOTEL_ID)
    VALUES ('R027', '2', 'No. of Beds: 1 King Bed; Apartment Size: 48 sqm; Apartment Facilities: Tea/Coffee maker, Minibar, TV, Telephone, Air conditioning, Hairdryer, Satellite Channels, Iron, Kitchenette,
                Refrigerator, Patio, Barbecue, Ironing facilities, Free toiletries, Toilet, Microwave, Washing machine, Private Bathroom, Heating, Cable channels, Bathtub or shower, Flat-screen TV, Soundproof,
                Dining area, Electric kettle, Kitchenware, Dryer, Wardrobe/Closet, Stovetop, Toaster, Towels, Linens, Dining table, Upper floors accessible by elevator,
                Drying rack for clothing, Toilet paper. Free WiFi!', 10,
            'Studio', 'Available', 'H008');

SELECT * FROM HOLIDAYBOOKINGAPPLICATION.ROOMS FETCH FIRST 100 ROWS ONLY;
