/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared.holidayBookingApp.repository;

import java.util.List;
import javax.ejb.Remote;
import shared.holidayBookingApp.entities.HolidayPackages;

/**
 *
 * @author Lucky
 */
@Remote
public interface HolidayPackagesRepository {
    
    /**
     * Return all the packages in the repository
     *
     * @return all the packages in the repository
     * @throws java.lang.Exception
     */
    public List<HolidayPackages> getAllPackages() throws Exception;
}
