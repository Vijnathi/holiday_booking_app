/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb.holidayBookingApp.repositoryImpl;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import shared.holidayBookingApp.entities.TransactionType;
import shared.holidayBookingApp.repository.TransactionTypeRepository;

/**
 *
 * @author Lucky
 */
@Stateless
public class TransactionTypeRepositoryImpl implements TransactionTypeRepository{

    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public void addTransactionType(TransactionType transactionType) throws Exception {
        entityManager.persist(transactionType);
    }

    @Override
    public TransactionType searchTransactionTypeById(int id) throws Exception {
        TransactionType transactionType = entityManager.find(TransactionType.class, id);
        return transactionType;
    }

    @Override
    public List<TransactionType> getAllTransactionType() throws Exception {
        return entityManager.createNamedQuery(TransactionType.GET_ALL_TRANSACTION_TYPES).getResultList();
    }

    @Override
    public void removeTransactionType(int transactionTypeId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void editTransactionType(TransactionType transactionType) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
