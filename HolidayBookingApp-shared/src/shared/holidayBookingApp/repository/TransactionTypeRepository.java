/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared.holidayBookingApp.repository;

import java.util.List;
import javax.ejb.Remote;
import shared.holidayBookingApp.entities.TransactionType;

/**
 *
 * @author Lucky
 */
@Remote
public interface TransactionTypeRepository {
    
    /**
     * Add the transactionType being passed as parameter into the repository
     *
     * @param transactionType - the transactionType to add
     * @throws java.lang.Exception
     */
    public void addTransactionType(TransactionType transactionType) throws Exception;
    
    
    /**
     * Search for a transaction type by its transaction type ID
     *
     * @param id - the transactionTypeId of the transaction type to search for
     * @return the transaction type found
     * @throws java.lang.Exception
     */
    public TransactionType searchTransactionTypeById(int id) throws Exception;
    
    /**
     * Return all the transaction types in the repository
     *
     * @return all the transaction types in the repository
     * @throws java.lang.Exception
     */
    public List<TransactionType> getAllTransactionType() throws Exception;
    
     /**
     * Remove the transaction type, whose transaction type ID matches the one being passed as parameter, from the repository
     *
     * @param transactionTypeId - the ID of the transaction type to remove
     * @throws java.lang.Exception
     */
    public void removeTransactionType(int transactionTypeId) throws Exception;
    
    /**
     * Update a transaction type in the repository
     *
     * @param transactionType - the updated information regarding a transaction type
     * @throws java.lang.Exception
     */
    public void editTransactionType(TransactionType transactionType) throws Exception;
    
}
