/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb.holidayBookingApp.repositoryImpl;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import shared.holidayBookingApp.entities.Helper;
import shared.holidayBookingApp.repository.HelperRepository;

/**
 *
 * @author Lucky
 */
public class HelperRepositoryImpl implements HelperRepository{

    ArrayList<Helper> h = new ArrayList<Helper>();

    
    @Override
    public void addHelper(Helper helper) throws Exception {
        h.add(helper);
    }

    @Override
    public List<Helper> getHelper() throws Exception {
        return h.subList(0, h.size());
    }
    
}
