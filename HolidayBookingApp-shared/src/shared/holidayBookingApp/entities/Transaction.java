/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared.holidayBookingApp.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Vijju
 */
@Entity(name = "Transactions")
@NamedQueries({
    @NamedQuery(name = Transaction.GET_ALL_TRANSACTIONS, query = "SELECT t FROM Transactions t")})
public class Transaction implements Serializable{
    
    public static final String GET_ALL_TRANSACTIONS = "Transaction.getAllTramsactions"; 
    private int transactionId;
    private String transactionName;
    private TransactionType transactionType;
    private String description;

    private User user;
    private HolidayPackages holidayPackages;
    
    public Transaction() {
    }

    public Transaction(int transactionId, String transactionName, TransactionType transactionType, String description, User user, HolidayPackages holidayPackages) {
        this.transactionId = transactionId;
        this.transactionName = transactionName;
        this.transactionType = transactionType;
        this.description = description;
        this.user = user;
        this.holidayPackages = holidayPackages;
    }

    @Id
    @Column(name = "TRANSACTION_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    @Column(name = "TRANSACTION_NAME")
    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    @ManyToOne
    @JoinColumn(name = "TRANSACTION_TYPE_ID")
    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(name = "PACKAGE_ID")
    public HolidayPackages getHolidayPackages() {
        return holidayPackages;
    }

    public void setHolidayPackages(HolidayPackages holidayPackages) {
        this.holidayPackages = holidayPackages;
    }
    
}
