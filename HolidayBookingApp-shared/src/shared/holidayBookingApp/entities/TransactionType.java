/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared.holidayBookingApp.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.TableGenerator;

/**
 *
 * @author Vijju
 */
@Entity
@NamedQueries({
    @NamedQuery(name = TransactionType.GET_ALL_TRANSACTION_TYPES, query = "SELECT tt FROM TransactionType tt")})
public class TransactionType implements Serializable{
    
    
    public static final String GET_ALL_TRANSACTION_TYPES = "TransactionType.getAllTransactionTypes"; 
    private long TransactionTypeId;
    private String TransactionTypeName;

    public TransactionType() {
    }

    public TransactionType(long TransactionTypeId, String TransactionTypeName) {
        this.TransactionTypeId = TransactionTypeId;
        this.TransactionTypeName = TransactionTypeName;
    }

    
    @TableGenerator(name = "transaction_id_generator", table = "transaction_id_generator", pkColumnName = "gen_name", valueColumnName = "gen_val", allocationSize = 100)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "transaction_id_generator")
    public long getTransactionTypeId() {
        return TransactionTypeId;
    }

    public void setTransactionTypeId(long TransactionTypeId) {
        this.TransactionTypeId = TransactionTypeId;
    }

    public String getTransactionTypeName() {
        return TransactionTypeName;
    }

    public void setTransactionTypeName(String TransactionTypeName) {
        this.TransactionTypeName = TransactionTypeName;
    }
    
}
