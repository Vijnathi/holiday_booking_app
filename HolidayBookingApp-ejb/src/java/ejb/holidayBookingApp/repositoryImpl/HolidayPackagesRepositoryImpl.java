/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb.holidayBookingApp.repositoryImpl;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import shared.holidayBookingApp.entities.HolidayPackages;
import shared.holidayBookingApp.repository.HolidayPackagesRepository;

/**
 *
 * @author Lucky
 */
@Stateless
public class HolidayPackagesRepositoryImpl implements HolidayPackagesRepository{

    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public List<HolidayPackages> getAllPackages() throws Exception {
        return entityManager.createNamedQuery(HolidayPackages.GET_ALL_HOLIDAY_PACKAGES).getResultList();
    }
    
}
