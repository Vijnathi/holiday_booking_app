/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holidayBookingApp.admin.mbeans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import shared.holidayBookingApp.entities.Helper;
import shared.holidayBookingApp.entities.User;
import shared.holidayBookingApp.repository.HelperRepository;
import shared.holidayBookingApp.repository.UserRepository;

/**
 *
 * @author Lucky
 */
@ManagedBean(name = "adminIndexManagedBean")
@SessionScoped
public class AdminIndexManagedBean implements Serializable{

    /**
     * Creates a new instance of AdminIndexManagedBean
     */
    private Helper helper;
    private String name;
    @EJB
    private HelperRepository helperRepository;
    @EJB
    private UserRepository userRepository;
    
    public AdminIndexManagedBean() {
        this.name="Welcoome";
//        init_name();
//        this.helper = new Helper();
//        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        username = request.getRemoteUser();
    }

    public Helper getHelper() {
        return helper;
    }

    public void setHelper(Helper helper) {
        this.helper = helper;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<Helper> getAllHelpers(){
        try {
            return helperRepository.getHelper();
        } catch (Exception ex) {
            Logger.getLogger(AdminIndexManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void init_name() {
        try {
            int userId = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName());
//            this.name = user.getFirstName() + " " + user.getLastName();
//            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

//        this.helper.setCurrentUser(request.getRemoteUser());

            this.name = Integer.toString(userId);
        } catch (Exception ex) {
            Logger.getLogger(AdminIndexManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
