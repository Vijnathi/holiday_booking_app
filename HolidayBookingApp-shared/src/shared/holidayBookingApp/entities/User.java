/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared.holidayBookingApp.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author Vijju
 */
@Entity(name = "Users")
@NamedQueries({
    @NamedQuery(name = User.GET_ALL_USERS, query = "SELECT u FROM Users u"),
    @NamedQuery(name = User.GET_LOGIN_USER, query = "SELECT u FROM Users u WHERE u.email = :username and u.password = :password")})

@SequenceGenerator(name="UserSequence",sequenceName = "USER_SEQ", initialValue=1, allocationSize=100)
public class User implements Serializable{
    
    public static final String GET_ALL_USERS = "User.getAllUsers";
    public static final String GET_LOGIN_USER = "User.getLoginUser";
    private int userId;
    private String lastName;
    private String firstName;
    private String email;
    private String password;
    private String type;
    private Address address;
    private String phoneNumber;
    Set<Transaction> transactions;

    public User() {
    }

    public User(String lastName, String firstName, String email, String password, String type, Address address, String phoneNumber) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.password = password;
        this.type = type;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.transactions = new HashSet<>();
    }

    @Override
    public String toString() {
        return "User{" + "lastName=" + lastName + ", firstName=" + firstName + ", email=" + email + ", password=" + password + ", type=" + type + ", address=" + address + ", phoneNumber=" + phoneNumber + '}';
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "UserSequence")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Embedded
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }    

    @OneToMany(mappedBy = "user")
    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }
    
}
