/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb.holidayBookingApp.repositoryImpl;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import shared.holidayBookingApp.entities.Transaction;
import shared.holidayBookingApp.repository.TransactionRepository;

/**
 *
 * @author Vijju
 */
@Stateless
public class TransactionRepositoryImpl implements TransactionRepository{
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public void addTransaction(Transaction transaction) throws Exception {
        entityManager.persist(transaction);
    }

    @Override
    public Transaction searchTransactionById(int id) throws Exception {
        Transaction transaction = entityManager.find(Transaction.class, id);
        return transaction;
    }

    @Override
    public List<Transaction> getAllTransactions() throws Exception {
        return entityManager.createNamedQuery(Transaction.GET_ALL_TRANSACTIONS).getResultList();
    }

    @Override
    public void removeTransaction(int transactionId) throws Exception {
        Transaction transaction = this.searchTransactionById(transactionId);

        if (transaction != null) {
            entityManager.remove(transaction);
        }
    }

    @Override
    public void editTransaction(Transaction transaction) throws Exception {
        entityManager.merge(transaction);
    }
}
