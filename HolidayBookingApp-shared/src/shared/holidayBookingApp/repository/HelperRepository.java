/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared.holidayBookingApp.repository;

import java.util.List;
import shared.holidayBookingApp.entities.Helper;

/**
 *
 * @author Lucky
 */
public interface HelperRepository {
    public void addHelper(Helper helper) throws Exception;
    public List<Helper> getHelper() throws Exception;
}
