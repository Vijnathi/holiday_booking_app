/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared.holidayBookingApp.entities;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;

/**
 *
 * @author Vijju
 */
@Entity
class Available implements Serializable{
    
    private int availabilityID;
    private Date startDate;
    private Date endDate;
    private int noOfDays;
    private double price;
    private String status;
    private HolidayPackages holidayPackage;

    public Available() {
    }

    public Available(int availabilityID, Date startDate, Date endDate, int noOfDays, double price, String status, HolidayPackages holidayPackage) {
        this.availabilityID = availabilityID;
        this.startDate = startDate;
        this.endDate = endDate;
        this.noOfDays = noOfDays;
        this.price = price;
        this.status = status;
        this.holidayPackage = holidayPackage;
    }
    
    @TableGenerator(name = "availability_id_generator", table = "availability_id_generator", pkColumnName = "gen_name", valueColumnName = "gen_val", allocationSize = 100)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "availability_id_generator")
    @Column(name = "AVAILABILITY_ID")
    public int getAvailabilityID() {
        return availabilityID;
    }

    public void setAvailabilityID(int availabilityID) {
        this.availabilityID = availabilityID;
    }

    @Column(name = "START_DATE")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Column(name = "END_DATE")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Column(name = "NO_OF_DAYS")
    public int getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(int noOfDays) {
        this.noOfDays = noOfDays;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @ManyToOne
    @JoinColumn(name = "PACKAGE_ID")
    public HolidayPackages getHolidayPackage() {
        return holidayPackage;
    }

    public void setHolidayPackage(HolidayPackages holidayPackage) {
        this.holidayPackage = holidayPackage;
    }
    
    
}
