/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb.holidayBookingApp.repositoryImpl;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import shared.holidayBookingApp.entities.Helper;
import shared.holidayBookingApp.entities.User;
import shared.holidayBookingApp.repository.UserRepository;

/**
 *
 * @author Vijnathi
 */
@Stateless
public class UserRepositoryImpl implements UserRepository{

    @PersistenceContext
    private EntityManager entityManager;
    private Helper helper;
    
    @Override
    public void addUser(User user) throws Exception {
        entityManager.persist(user);
        entityManager.flush();
//        helper.setCurrentUser(user.getUserId());
    }

    @Override
    public User findUser(String username, String password) throws Exception {
        return (User) this.entityManager.createNamedQuery(User.GET_LOGIN_USER).setParameter("username",username).setParameter("password", password).getSingleResult();
    }

    @Override
    public User searchUserById(int id) throws Exception {
        User user = entityManager.find(User.class, id);
        return user;
    }

    @Override
    public List<User> getAllUsers() throws Exception {
        return entityManager.createNamedQuery(User.GET_ALL_USERS).getResultList();
    }

    @Override
    public void removeUser(int userId) throws Exception {
        User user = this.searchUserById(userId);

        if (user != null) {
            entityManager.remove(user);
        }
    }

    @Override
    public void editUser(User user) throws Exception {
        entityManager.merge(user);
    }
    
}
